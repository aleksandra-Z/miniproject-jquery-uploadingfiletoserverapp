$(document).ready(function(){
    
//   window.addEventListener("dragover",function(e){
//     e.preventDefault();
//   },false);
//   window.addEventListener("drop",function(e){
//     e.preventDefault();
//   },false);   
//  
   $("#upload").dragover(function(e){
      e.preventDefault(); 
   })
   .drop(function(e){
       e.preventDefault();
       var files = e.dataTransfer.files;
       if (files.length){
           var file = files[0];
           reader = new FileReader();
           reader.readAsDataURL(file);
           reader.onloadend = function(e){
               onLoadEndHandler(e, file);
           }
       }
   }); 
   
   function onLoadEndHandler(e, file){
       $("#preview-image").attr("src", e.target.result);
       $("#file-name").text("Name: "+file.name);
       $("#file-type").text("Type: "+file.type);
       $("#file-size").text("Size: "+file.size);

   }
   
});